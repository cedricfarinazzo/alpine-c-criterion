FROM alpine
LABEL authors="Cédric Farinazzo <cedrc.farinazzo@gmail.com>"

#Base installation
RUN apk update --no-cache
RUN apk upgrade --no-cache

RUN apk add --no-cache wget curl
RUN apk add --no-cache musl-dev gcc clang g++ 
RUN apk add --no-cache tar git make cmake valgrind 

ENV criterion_ver=2.3.3
RUN cd /tmp && wget https://github.com/Snaipe/Criterion/releases/download/v${criterion_ver}/criterion-v${criterion_ver}.tar.bz2 
RUN cd /tmp && tar xjf criterion-v${criterion_ver}.tar.bz2 && \
    cd "/tmp/criterion-v${criterion_ver}" && \
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=/usr -DCTESTS=ON -S . -B build && \
    cmake --build build && \
    cmake --build build --target install

